package com.homeplus.exceptions;

public class JPAOptException extends Exception {
    public JPAOptException(String message) {
        super(message);
    }
}
