package com.homeplus.dtos.comment;

import com.homeplus.models.TaskEntity;
import com.homeplus.models.UserEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@Data
@NoArgsConstructor
public class CommentGetDto {
    private Long id;

    private UserEntity UserEntity;

    private TaskEntity taskEntity;

    private String message;

    private OffsetDateTime created_time;

    private OffsetDateTime updated_time;
}
