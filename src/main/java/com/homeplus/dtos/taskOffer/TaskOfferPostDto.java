package com.homeplus.dtos.taskOffer;

import com.homeplus.models.TaskEntity;
import com.homeplus.models.TaskOfferStatus;
import com.homeplus.models.TaskerEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TaskOfferPostDto {
    private Long tasker_id;

    private Long task_id;

    private TaskerEntity taskerEntity;

    private TaskEntity taskEntity;

    private String offered_price;

    private String description;

    private final TaskOfferStatus offer_status = TaskOfferStatus.PENDING;

    private final OffsetDateTime created_time = OffsetDateTime.now();

    private final OffsetDateTime updated_time = OffsetDateTime.now();
}
