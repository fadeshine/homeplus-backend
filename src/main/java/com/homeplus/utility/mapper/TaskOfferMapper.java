package com.homeplus.utility.mapper;

import com.homeplus.dtos.task.TaskGetDto;
import com.homeplus.dtos.task.TaskPostDto;
import com.homeplus.dtos.task.TaskPutDto;
import com.homeplus.dtos.taskOffer.TaskOfferGetDto;
import com.homeplus.dtos.taskOffer.TaskOfferPostDto;
import com.homeplus.dtos.taskOffer.TaskOfferPutDto;
import com.homeplus.models.TaskEntity;
import com.homeplus.models.TaskOfferEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface TaskOfferMapper {

    TaskOfferEntity postDtoToEntity(TaskOfferPostDto taskOfferPostDto);

    TaskOfferEntity putDtoToEntity(TaskOfferPutDto taskOfferPutDto);

    TaskOfferGetDto fromEntity(TaskOfferEntity taskOfferEntity);
}
